
package com.bap.jp.seminar.api.user;

/*
 * User controller
 * @author tuyennv
 * 7/5/2019
 */

import com.bap.jp.seminar.common.exception.DataConflictException;
import com.bap.jp.seminar.common.exception.PagingException;
import com.bap.jp.seminar.common.exception.ResourceNotFoundException;
import com.bap.jp.seminar.common.model.User;
import com.bap.jp.seminar.common.response.ErrorResponse;
import com.bap.jp.seminar.common.response.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * Find all.
     * code: API-11
     * @return ResponseEntity
     */

    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity findAll( @RequestParam(name = "page", required = false, defaultValue = "1") Integer page,
                                   @RequestParam(name = "pageSize", required = false, defaultValue = "5") Integer pageSize) throws PagingException{

        if (page < 1) {
            throw new PagingException("Page index must not be less than zero!");
        } else if (pageSize < 1){
            throw new PagingException("Page size must not be less than one!");
        }
        PageRequest request = PageRequest.of(page - 1, pageSize);
        return new ResponseEntity<>(userService.findAll(request), HttpStatus.OK);
    }



    /**
     * Create new User.
     * bCryptPasswordEncoder: ecoder for password
     * check username ,check email
     * code: API-6
     * @param user
     * @return ResponseEntity
     * @throws DataConflictException
     */
    @PostMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity create(@Valid @RequestBody User user) throws DataConflictException {

        if (userService.existsByUsername(user.getUsername())
                || userService.existsByEmail(user.getEmail())) {
            throw  new DataConflictException("UserName or email Already Exist!"+ user.getEmail());
        }
        String pw = user.getPassword();
        user.setPassword(bCryptPasswordEncoder.encode(pw));
        Integer id=userService.saveUser(user);
        if(id!=-1){
            SuccessResponse response = new SuccessResponse("Successfully created new student."+id);
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        }
        ErrorResponse response = new ErrorResponse("created student not success.");
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }



    /**
     * admin Update role for user.
     * code: API-8,API-7
     * @param id
     * @param user
     * @return ResponseEntity
     * @throws ResourceNotFoundException
     */
    @PutMapping(value = {"/{id}"})
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity updateUser(@Valid @PathVariable Integer id, @RequestBody User user) throws ResourceNotFoundException{

        if(userService.updateUser(id,user)){
            SuccessResponse response = new SuccessResponse("Successfully update user.");
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        }
        throw new ResourceNotFoundException("user id '" + id+ "' does no exist");
    }


    /**
     * Find by id.
     * code: API-9
     * @param userId
     * @return User
     * @throws Exception
     */
    @GetMapping(value = {"/{id}"})
    public User findById(@Valid @PathVariable("id") Integer userId) throws Exception{
        return userService.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User id '" + userId + "' does no exist"));
    }



    /**
     * admin Delete userC
     * code:API-12
     * @param userId
     * @return ResponseEntity
     * @throws ResourceNotFoundException
     */

    @DeleteMapping(value = {"/{id}"})
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity deleteById(@PathVariable("id") Integer userId) throws ResourceNotFoundException{
        SuccessResponse response;
        if( userService.findById(userId).isPresent()){
            userService.deleteById(userId);
            response = new SuccessResponse("Successfully deleted user id '" + userId + "'");
        } else{
            throw new ResourceNotFoundException("Student id '" + userId + "' does no exist");
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


}

