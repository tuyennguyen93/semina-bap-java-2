
package com.bap.jp.seminar.api.authentication;


import com.bap.jp.seminar.common.exception.InternalServerException;
import com.bap.jp.seminar.common.repository.UserRepository;
import com.bap.jp.seminar.config.security.UserPrinciple;
import com.bap.jp.seminar.config.security.jwt.JwtProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


@RestController
//@CrossOrigin
@RequestMapping("/auth")
public class AuthenticationController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;


    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtProvider jwtProvider;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginForm loginRequest) {

        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            loginRequest.getUsername(),
                            loginRequest.getPassword()
                    )
            );
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtProvider.generateJwtToken(authentication);
            //role
            Short role = ((UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getRole();

            return ResponseEntity.ok(new JwtResponse(jwt, role));
        } catch (Exception e) {
            throw new InternalServerException("Cannot login ", e.getCause());
        }

    }

//    @PostMapping("/signup")
//    public ResponseEntity<String> registerUser(@Valid @RequestBody SignUpForm signUpRequest) {
//        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
//            return new ResponseEntity<String>("Fail -> Username is already taken!",
//                    HttpStatus.BAD_REQUEST);
//        }
//
//        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
//            return new ResponseEntity<String>("Fail -> Email is already in use!",
//                    HttpStatus.BAD_REQUEST);
//        }
//
//        // Creating userC's account
//        User userC = new User(signUpRequest.getName(), signUpRequest.getUsername(),
//                signUpRequest.getEmail(), encoder.encode(signUpRequest.getPassword()),
//                signUpRequest.getRole(),);
//
//        userRepository.save(userC);
//
//        return ResponseEntity.ok().body("User registered successfully!");
//    }
}