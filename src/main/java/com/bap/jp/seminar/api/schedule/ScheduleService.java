package com.bap.jp.seminar.api.schedule;

import com.bap.jp.seminar.common.exception.InternalServerException;
import com.bap.jp.seminar.common.model.Schedule;
import com.bap.jp.seminar.common.repository.ScheduleRegisterRepository;
import com.bap.jp.seminar.common.repository.ScheduleRepository;
import com.bap.jp.seminar.common.utils.SerminarUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author tuyennv
 * 7/9/2019
 */
@Service
public class ScheduleService {
    @Autowired
    ScheduleRepository scheduleRepository;

    @Autowired
    ScheduleRegisterRepository scheduleRegisterRepository;

    /**
     * Find all Schedule
     */
    public List<Schedule> findAll() {
        List<Schedule> schedules = scheduleRepository.findAll();
        schedules.forEach(schedule -> {
            Float rate = scheduleRegisterRepository.statisticalRate(schedule.getId());
            schedule.setAvgRate(rate);
            if (scheduleRegisterRepository.findByUserIdAndScheduleId(SerminarUtils.getCurrentUserId(), schedule.getId()).isPresent() ||
                    !schedule.getTimeStart().isAfter(java.time.LocalDateTime.now())) {
                schedule.setJoiner(true);
            } else
                schedule.setJoiner(false);
        });
        return schedules;
    }


    /**
     * find Schedule by id
     */
    public Optional<Schedule> findById(Integer id) {
        return scheduleRepository.findById(id);
    }

    /**
     * create new Schedule & update Schedule
     */
    public void save(Schedule schedule) {
        try {
            scheduleRepository.save(schedule);
        } catch (Exception e) {
            throw new InternalServerException("Cannot create topic", e.getCause());
        }
    }


    /**
     * delete chedule
     */
    public void deleteById(Integer scheduleId) {
        try {
            scheduleRepository.deleteById(scheduleId);
        } catch (Exception e) {
            throw new InternalServerException("Cannot delete topic", e.getCause());
        }

    }
}
