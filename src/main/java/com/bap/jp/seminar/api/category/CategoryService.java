package com.bap.jp.seminar.api.category;


import com.bap.jp.seminar.common.exception.InternalServerException;
import com.bap.jp.seminar.common.model.Category;
import com.bap.jp.seminar.common.model.Topic;
import com.bap.jp.seminar.common.repository.CategoryRepository;
import com.bap.jp.seminar.common.repository.TopicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author tuyennv
 * 7/6/2019
 */
@Service
public class CategoryService {
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    TopicRepository topicRepository;

    //find all
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    //find by id
    public Optional<Category> findById(Integer id) {
       return categoryRepository.findById(id);
    }
    //check name
    public boolean existsByName(String name) {
        if (categoryRepository.existsByName(name)) {
            return true;
        }
        return false;
    }
    public Integer save(Category category) {
        try {

            categoryRepository.save(category);
            Optional<Category> optionalCategory = categoryRepository.findByName(category.getName());
            if (optionalCategory.isPresent()) {
                return optionalCategory.get().getId();
            }
            return -1;
        } catch (Exception e){
            throw new InternalServerException("Cannot create topic", e.getCause());
        }
    }

    public Boolean update(Integer id,Category categoryDetail) {
        Optional<Category> optionalCategory=categoryRepository.findById(id);
        if(optionalCategory.isPresent()){
            optionalCategory.get().setName(categoryDetail.getName());

            try {
                categoryRepository.save(optionalCategory.get());
            } catch (Exception e){
                throw new InternalServerException("Cannot update category", e.getCause());
            }
            return true;
        }
        return false;
    }

    public void deleteById(Integer topicId) {
        try {
            categoryRepository.deleteById(topicId);
        } catch (Exception e){
            throw new InternalServerException("Cannot delete category", e.getCause());
        }

    }


}
