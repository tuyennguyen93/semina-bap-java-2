package com.bap.jp.seminar.api.schedule;

import com.bap.jp.seminar.common.exception.DataConflictException;
import com.bap.jp.seminar.common.exception.ResourceNotFoundException;
import com.bap.jp.seminar.common.model.Schedule;
import com.bap.jp.seminar.common.response.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * @author tuyennv
 * 7/9/2019
 */
@RestController
@RequestMapping("/schedules")
public class ScheduleController {
    @Autowired
    private ScheduleService scheduleService;

    /**
     * Find All
     * code:API-30,API-32
     */
    @GetMapping
    public ResponseEntity findAll() {
        return new ResponseEntity<>(scheduleService.findAll(), HttpStatus.OK);
    }

    /**
     * Find Topic by id.
     * code:API-31,API-33
     */
    @GetMapping(value = {"/{id}"})
    public Schedule findById(@Valid @PathVariable("id") Integer id) throws Exception{
        return scheduleService.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("schedule id '" + id + "' does no exist"));

    }

    /**
     * admin create a new schedule
     * code:API-29
     */
    @PostMapping
    @PreAuthorize( "hasRole('ROLE_ADMIN')")
    public ResponseEntity create(@RequestBody Schedule schedule) throws DataConflictException {
            scheduleService.save(schedule);
        SuccessResponse response = new SuccessResponse("Successfully created new schedule." );
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    /**
     * admin Update Schedule
     * code:API-28
     */
    @PutMapping(value = {"/{id}"})
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity update(@PathVariable Integer id,@RequestBody Schedule schedule) throws ResourceNotFoundException{
            scheduleService.save(schedule);
        SuccessResponse response = new SuccessResponse("Successfully update schedule.");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    /**
     * admin delete schedule
     * @param id
     * @return
     * @throws ResourceNotFoundException
     */
    @DeleteMapping(value = {"/{id}"})
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity deleteById(@PathVariable("id") Integer id) throws ResourceNotFoundException {
        SuccessResponse response;
        if (scheduleService.findById(id).isPresent()) {
            scheduleService.deleteById(id);
            response = new SuccessResponse("Successfully deleted Schedule id '" + id + "'");
        } else{
            throw new ResourceNotFoundException("Schedule id '" + id + "' does no exist");
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }



}
