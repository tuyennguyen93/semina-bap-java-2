package com.bap.jp.seminar.api.authentication;

public class JwtResponse {
    private String token;
    private String type = "Bearer";
    private Short role;
    public JwtResponse(String accessToken,Short role) {
        this.token = accessToken;
        this.role=role;
    }

    public String getAccessToken() {
        return token;
    }

    public void setAccessToken(String accessToken) {
        this.token = accessToken;
    }

    public String getTokenType() {
        return type;
    }

    public void setTokenType(String tokenType) {
        this.type = tokenType;
    }

    public Short getRole() {
        return role;
    }

    public void setRole(Short role) {
        this.role = role;
    }
}