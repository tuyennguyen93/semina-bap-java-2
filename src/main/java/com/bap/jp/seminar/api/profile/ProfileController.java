package com.bap.jp.seminar.api.profile;

import com.bap.jp.seminar.api.profile.profiledto.PassDTO;
import com.bap.jp.seminar.common.exception.DataConflictException;
import com.bap.jp.seminar.common.exception.ResourceNotFoundException;
import com.bap.jp.seminar.common.model.User;
import com.bap.jp.seminar.common.response.ErrorResponse;
import com.bap.jp.seminar.common.response.SuccessResponse;
import com.bap.jp.seminar.common.utils.SerminarUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author tuyennv
 * 7/11/2019
 */
@RestController
@RequestMapping("/profile")
public class ProfileController {

    @Autowired
    private ProfileService profileService;


    /**
     * User Update profile .
     * code: API-1
     * @param user
     * @return ResponseEntity
     * @throws ResourceNotFoundException
     */

    @PutMapping
    public ResponseEntity updateProfile( @RequestBody User user) throws ResourceNotFoundException {
        profileService.updateProfile(user);
        SuccessResponse response = new SuccessResponse("Successfully update profile.");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    /**
     * get profile by user current.
     * code: API-3
     * @return User
     * @throws Exception
     */
    @GetMapping
    public User getProfile() throws Exception {
        return profileService.findById(SerminarUtils.getCurrentUserId())
                .orElseThrow(() -> new ResourceNotFoundException("profile id does no exist"));

    }


    /**
     * change password
     * old pass and new pass
     * code:AIP-2 , API-10
     * @param pass
     * @return ResponseEntity
     */
    @PutMapping(value = {"/changepassword"})
    public ResponseEntity changePassword(@Valid @RequestBody PassDTO pass) throws DataConflictException {
        profileService.changePassword(pass);
            SuccessResponse response = new SuccessResponse("Successfully update profile.");
            return new ResponseEntity<>(response, HttpStatus.OK);

    }

}
