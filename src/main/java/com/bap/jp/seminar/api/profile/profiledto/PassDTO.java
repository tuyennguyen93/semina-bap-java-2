package com.bap.jp.seminar.api.profile.profiledto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * pass
 * @author tuyennv
 * 7/5/2019
 */
@Getter
@Setter
@NoArgsConstructor
public class PassDTO {
    private String oldPass;
    private String newPass;

}
