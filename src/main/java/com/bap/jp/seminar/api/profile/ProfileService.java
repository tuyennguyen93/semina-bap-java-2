package com.bap.jp.seminar.api.profile;

import com.bap.jp.seminar.api.profile.profiledto.PassDTO;
import com.bap.jp.seminar.common.exception.DataConflictException;
import com.bap.jp.seminar.common.exception.InternalServerException;
import com.bap.jp.seminar.common.exception.ResourceNotFoundException;
import com.bap.jp.seminar.common.model.User;
import com.bap.jp.seminar.common.repository.UserRepository;
import com.bap.jp.seminar.common.utils.SerminarUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author tuyennv
 * 7/11/2019
 */

@Service
public class ProfileService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * update profile (name,major)
     */
    public void updateProfile(User userDetail) throws ResourceNotFoundException {
        Optional<User> optionalUser=userRepository.findById(SerminarUtils.getCurrentUserId());
        if(optionalUser.isPresent()){
            User user=optionalUser.get();
            user.setName(userDetail.getName());
            user.setMajor(userDetail.getMajor());
            try {
                userRepository.save(user);
            }catch (Exception e){
                throw new InternalServerException("Cannot save user", e.getCause());
            }
        } else
        throw new ResourceNotFoundException("Profile  does no exist");
    }

    /**
     * Find User by id.
     */
    public Optional<User> findById(Integer id) {
        return  userRepository.findById(id);
    }

    /**
     * change password
     */
    void changePassword(PassDTO pass) throws DataConflictException {
        Optional<User> user = userRepository.findById(SerminarUtils.getCurrentUserId());
        if (user.isPresent()) {
            String encoder = user.get().getPassword();
            Boolean check = bCryptPasswordEncoder.matches(pass.getOldPass(), encoder);
            if (check) {
                user.get().setPassword(bCryptPasswordEncoder.encode(pass.getNewPass()));
                try {
                    userRepository.save(user.get());
                } catch (Exception e){
                    throw new InternalServerException("Cannot change password", e.getCause());
                }

            } else {
                throw  new DataConflictException("wrong old password.");
            }
        }

    }
}
