package com.bap.jp.seminar.api.topic;

import com.bap.jp.seminar.common.exception.DataConflictException;
import com.bap.jp.seminar.common.exception.ResourceNotFoundException;
import com.bap.jp.seminar.common.model.Topic;
import com.bap.jp.seminar.common.response.ErrorResponse;
import com.bap.jp.seminar.common.response.SuccessResponse;
import com.bap.jp.seminar.common.utils.SerminarUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
/*
 * User controller
 * @author tuyennv
 * 7/8/2019
 */

@RestController
@RequestMapping("/topics")
public class TopicController {
    @Autowired
    private TopicService topicService;


    /**
     * Find all TopicDTO.(hidden category)
     * code :API-23
     * @return ResponseEntity
     */
    @GetMapping(value = {"/dto"})
    public ResponseEntity findAllDTO() {
        return new ResponseEntity<>(topicService.findAll(), HttpStatus.OK);
    }


    /**
     * Find all Topic & [Category].
     * code :API-23
     * @return ResponseEntity
     */
    @GetMapping
    public ResponseEntity findAll() {
        return new ResponseEntity<>(topicService.findAll(), HttpStatus.OK);
    }



    /**
     * Find by id.
     * code:API-24
     * @param topicId
     * @return Topic
     * @throws Exception
     */
    @GetMapping(value = {"/{id}"})
    public Topic findById(@Valid @PathVariable("id") Integer topicId) throws Exception {
        return topicService.findById(topicId)
                .orElseThrow(() -> new ResourceNotFoundException("topics id '" + topicId + "' does no exist"));

    }


    /**
     * Create
     * code :API-20,API-26
     * @param topic new Topic.
     * @return ResponseEntity
     * @throws DataConflictException
     */

    @PostMapping
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public ResponseEntity create(@Valid @RequestBody Topic topic) throws DataConflictException {
        if (SerminarUtils.isCurrentUserAdmin()) {
            topic.setStatus((short) 1);
        } else {
            topic.setStatus((short) 0);
        }

        topic.setUserId(SerminarUtils.getCurrentUserId());
        if (!topicService.existsByTitle(topic.getTitle())) {
            Integer rs = topicService.save(topic);
            if (rs != -1) {
                SuccessResponse response = new SuccessResponse("Successfully created new topic." + rs);
                return new ResponseEntity<>(response, HttpStatus.CREATED);
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        throw new DataConflictException("Topic is already in use: ");
    }


    /**
     * Topic Update (admin apply topic)
     * code:API-21,API-25
     * @param id
     * @param topic
     * @return ResponseEntity
     */
    @PutMapping(value = {"/{id}"})
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity updateProfile(@Valid @PathVariable Integer id, @RequestBody Topic topic) {
        if (topicService.update(id, topic)) {
            SuccessResponse response = new SuccessResponse("Successfully update topic.");
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        ErrorResponse response = new ErrorResponse("Topic does no exist.");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    /**
     * get All approve Topic,deny Topic
     * @param status
     * @return
     */
    @GetMapping(value = {"/status/{status}"})
    public ResponseEntity findAllByStatus(@Valid @PathVariable Short status) {
       return new ResponseEntity<> (topicService.findAllByStatus((short) status),HttpStatus.OK);
    }



    /**
     * get All topic myserft(yourserf create)
     * @return ResponseEntity
     */
    @GetMapping(value = {"/mine"})
    public ResponseEntity findAllByUserId() {
        Integer idUser = SerminarUtils.getCurrentUserId();
        return new ResponseEntity<> (topicService.findAllByUserId(idUser),HttpStatus.OK);
    }



    /**
     * admin Delete Topic
     * code:API-22
     * @param topicId
     * @return
     * @throws ResourceNotFoundException
     */
    @DeleteMapping(value = {"/{id}"})
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity deleteById(@PathVariable("id") Integer topicId) throws ResourceNotFoundException {
        SuccessResponse response;
        if (topicService.findById(topicId).isPresent()) {
            topicService.deleteById(topicId);
            response = new SuccessResponse("Successfully deleted Topic id '" + topicId + "'");
        } else{
            throw new ResourceNotFoundException("Topic id '" + topicId + "' does no exist");
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


}
