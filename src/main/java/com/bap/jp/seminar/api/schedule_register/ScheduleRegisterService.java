package com.bap.jp.seminar.api.schedule_register;

import com.bap.jp.seminar.api.schedule_register.ratedto.RateDTO;
import com.bap.jp.seminar.common.exception.InternalServerException;
import com.bap.jp.seminar.common.model.ScheduleRegister;
import com.bap.jp.seminar.common.model.User;
import com.bap.jp.seminar.common.repository.ScheduleRegisterRepository;
import com.bap.jp.seminar.common.repository.ScheduleRepository;
import com.bap.jp.seminar.common.repository.UserRepository;
import com.bap.jp.seminar.common.response.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author tuyennv
 * 7/9/2019
 */

@Service
public class ScheduleRegisterService {
    @Autowired
    ScheduleRegisterRepository scheduleRegisterRepository;

    @Autowired
    ScheduleRepository scheduleRepository;

    @Autowired
   UserRepository userRepository;

    /**
     * find All schedule register
     */
    public List<ScheduleRegister> findAll() {
        return scheduleRegisterRepository.findAll();
    }

    /**
     * create (register) schedule for user
     */
    public  void save(ScheduleRegister scheduleRegister){
        try {
            scheduleRegisterRepository.save(scheduleRegister);
        }catch (Exception e){
            throw new InternalServerException("Cannot create topic", e.getCause());
        }
    }
    /*
     * user rate for schedule
     */
    public Boolean rate(Integer rate, Integer user_id, Integer schedule_id) {

       if( scheduleRegisterRepository.findByUserIdAndScheduleId(user_id,schedule_id).isPresent()) {
           try {
               scheduleRegisterRepository.rate(rate, user_id, schedule_id);
               return true;
           } catch (Exception e) {
               throw new InternalServerException("Cannot rate ", e.getCause());
           }
       } else return false;
    }

    /**
     * statistical rate
     */
    public List<RateDTO> statisticalRates() {
        return scheduleRegisterRepository.statisticalRates();
    }

    /**
     * statistical rate by id schedule
     */
    public Float statisticalRate(Integer idSchedule) {
        return scheduleRegisterRepository.statisticalRate(idSchedule);
    }
    /**
     * find by id
     */
    public Optional<ScheduleRegister> findById(Integer id) {
        return scheduleRegisterRepository.findById(id);
    }

    /**
     * delete by id
     */
    public void deleteById(Integer id) {
        try {
            scheduleRegisterRepository.deleteById(id);
        } catch (Exception e){
            throw new InternalServerException("Cannot delete Register schedule", e.getCause());
        }

    }

    /**
     * find schedule register by id schedule
     */
    public List<ScheduleRegister> findAllByScheduleId(Integer scheduleId) {
        List<ScheduleRegister> scheduleRegisters=scheduleRegisterRepository.findAllByScheduleId(scheduleId);
        scheduleRegisters.forEach(schedule -> {
            Optional<User> user= userRepository.findById(schedule.getUserId());
            if(user.isPresent()) {
                schedule.setUserName(user.get().getUsername());
            }
        });
        return scheduleRegisters;
    }
    /**
     * find schedule register by id user
     */
    public List<ScheduleRegister> findAllByUserId(Integer userId) {
        List<ScheduleRegister> scheduleRegisters=scheduleRegisterRepository.findAllByUserId(userId);
        return scheduleRegisters;
    }


}
