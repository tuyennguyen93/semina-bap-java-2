package com.bap.jp.seminar.api.topic;

import com.bap.jp.seminar.api.topic.topicdto.TopicDTO;
import com.bap.jp.seminar.common.exception.InternalServerException;
import com.bap.jp.seminar.common.model.Topic;
import com.bap.jp.seminar.common.repository.TopicRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TopicService {
    @Autowired
    TopicRepository topicRepository;

    /**
     * Find all TopicDTO.
     */
    public List<TopicDTO> findAllDTO() {

        List<Topic> topics = topicRepository.findAll();
        List<TopicDTO> topicDTOS = new ArrayList<>();
        for (Topic topic : topics) {
            TopicDTO topicDTO = new TopicDTO();
            BeanUtils.copyProperties(topic, topicDTO);
            topicDTOS.add(topicDTO);
        }
        return topicDTOS;
    }

    /**
     * Find all TopicDTO.
     */
    public List<Topic> findAll() {
        return topicRepository.findAll();
    }


    /**
     * Find Topic by id.
     */
    public Optional<Topic> findById(Integer id) {
        return  topicRepository.findById(id);
    }

    /**
     * create new the Topic.
     * @Return id
     */
    public Integer save(Topic topic) {
        try {

            topicRepository.save(topic);
            Optional<Topic> optionalTopic = topicRepository.findByTitle(topic.getTitle());
            if (optionalTopic.isPresent()) {
                return optionalTopic.get().getId();
            }
            return -1;
        } catch (Exception e){
            throw new InternalServerException("Cannot create topic", e.getCause());
        }
    }
    /**
     * update topic
     */
    public Boolean update(Integer id,Topic topicDetail) {
        Optional<Topic> optionalTopic=topicRepository.findById(id);
        if(optionalTopic.isPresent()){
            optionalTopic.get().setTitle(topicDetail.getTitle());
            optionalTopic.get().setUserId(topicDetail.getUserId());
            optionalTopic.get().setStatus(topicDetail.getStatus());
            optionalTopic.get().setDescription(topicDetail.getDescription());
            optionalTopic.get().setCategories(topicDetail.getCategories());
            try {
                topicRepository.save(optionalTopic.get());
            } catch (Exception e){
                throw new InternalServerException("Cannot save topic", e.getCause());
            }
            return true;
        }
        return false;
    }

    /**
     * check title name
     */
    public boolean existsByTitle(String title) {
        if (topicRepository.existsByTitle(title)) {
            return true;
        }
        return false;
    }
    /**
     * get all topic by status (approve,deny)
     */
    public List<Topic> findAllByStatus(Short status){
        return topicRepository.findAllByStatus(status);
    }

    /**
     * get all topic by user create(yourseft create)
     */
    public List<Topic> findAllByUserId(Integer id){
        return topicRepository.findAllByUserId(id);
    }

    /**
     * Delete the Topic.
     */
    public void deleteById(Integer topicId) {
        try {
            topicRepository.deleteById(topicId);
        } catch (Exception e){
            throw new InternalServerException("Cannot delete topic", e.getCause());
        }

    }

}