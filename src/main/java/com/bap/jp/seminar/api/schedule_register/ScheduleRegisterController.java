package com.bap.jp.seminar.api.schedule_register;


import com.bap.jp.seminar.api.schedule_register.ratedto.RateDTO;
import com.bap.jp.seminar.api.schedule_register.ratedto.ScheduleRegisterDTO;
import com.bap.jp.seminar.common.exception.DataConflictException;
import com.bap.jp.seminar.common.exception.ResourceNotFoundException;
import com.bap.jp.seminar.common.model.ScheduleRegister;
import com.bap.jp.seminar.common.response.SuccessResponse;
import com.bap.jp.seminar.common.utils.SerminarUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author tuyennv
 * 7/9/2019
 */
@RestController
@RequestMapping("/registerSchedules")
public class ScheduleRegisterController {
    @Autowired
    private ScheduleRegisterService scheduleRegisterService;

    /**
     * find All schedule register
     * code: API-36
     */
    @GetMapping
    public ResponseEntity findAll() {
        return new ResponseEntity<>(scheduleRegisterService.findAll(), HttpStatus.OK);
    }


    /**
     * create (register) schedule for user
     * code: API-34
     * node: function no use
     */
    @PostMapping
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public ResponseEntity create(@RequestBody ScheduleRegisterDTO scheduleRegisterDTO ) throws DataConflictException {
        ScheduleRegister scheduleRegister= new ScheduleRegister();
        scheduleRegister.setScheduleId(scheduleRegisterDTO.getScheduleId());
        scheduleRegister.setUserId(SerminarUtils.getCurrentUserId());
        scheduleRegisterService.save(scheduleRegister);
        SuccessResponse response = new SuccessResponse("Successfully register schedule.");
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    /**
     * user rate for schedule
     * code:API-35
     */
    @PutMapping
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public ResponseEntity rate(
            @RequestBody RateDTO rate
    ) {
        Integer userId= SerminarUtils.getCurrentUserId();
        if (rate.getRate() >= 1 && rate.getRate() <= 5) {
            Boolean rs= scheduleRegisterService.rate(rate.getRate(), userId, rate.getScheduleId());
            if(rs) {
                SuccessResponse response = new SuccessResponse("Successfully update rate.");
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        }
        SuccessResponse response = new SuccessResponse("Error update rate.");
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);

    }


    /**
     * find schedule register by id schedule
     */
    @GetMapping("/listUser/{scheduleId}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public ResponseEntity findAllByScheduleId(@PathVariable Integer scheduleId) {
        return new ResponseEntity<>(scheduleRegisterService.findAllByScheduleId(scheduleId), HttpStatus.OK);
    }

    /**
     * find schedule register by id user
     */
    @GetMapping("/listSchedule")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public ResponseEntity findAllByUserId() {
        Integer userId= SerminarUtils.getCurrentUserId();
        return new ResponseEntity<>(scheduleRegisterService.findAllByUserId(userId), HttpStatus.OK);
    }

    /**
     * delete schedule register
     */
    @DeleteMapping(value = {"/{id}"})
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public ResponseEntity deleteById(@PathVariable("id") Integer id) throws ResourceNotFoundException {
        SuccessResponse response;
        if (scheduleRegisterService.findById(id).isPresent()) {
            scheduleRegisterService.deleteById(id);
            response = new SuccessResponse("Successfully deleted ScheduleRegisterID  '" + id + "'");
        } else{
            throw new ResourceNotFoundException("ScheduleRegisterID '" + id + "' does no exist");
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
