package com.bap.jp.seminar.api.user;

import com.bap.jp.seminar.common.exception.InternalServerException;
import com.bap.jp.seminar.common.model.User;
import com.bap.jp.seminar.common.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * find all
     */

    public Page<User> findAll(Pageable pageRequest) {
        return userRepository.findAll(pageRequest);
    }



    /**
     * Save the User.
     */
    public void save(User user) {
        try {
            userRepository.save(user);
        } catch (Exception e) {
            throw new InternalServerException("Cannot save user", e.getCause());
        }

    }

    /**
     * Delete the User.
     */
    public void deleteById(Integer userId) {
        try {
            userRepository.deleteById(userId);
        } catch (Exception e) {
            throw new InternalServerException("Cannot delete user", e.getCause());
        }

    }

    /**
     * Find User by id.
     */
    public Optional<User> findById(Integer id) {
        return userRepository.findById(id);
    }


    // check user
    Boolean existsByUsername(String userName) {
        return userRepository.existsByUsername(userName);
    }

    //check email
    Boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    //create user
    Integer saveUser(User user) {
        try {
            userRepository.save(user);
            Optional<User> userOptional = userRepository.findByEmail(user.getEmail());
            if (userOptional.isPresent()) {
                return userOptional.get().getId();
            }
            return -1;
        } catch (Exception e) {
            throw new InternalServerException("Cannot save user", e.getCause());
        }
    }

    //admin update profile
    Boolean updateUser(Integer id, User userDetail) {

        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            user.setName(userDetail.getName());
            user.setEmail(userDetail.getEmail());
            user.setRole(userDetail.getRole());
            user.setMajor(userDetail.getMajor());
            try {
                userRepository.save(user);
                return true;
            }catch (Exception e){
                throw new InternalServerException("Cannot save user", e.getCause());
            }
        }
        return false;
    }
}

