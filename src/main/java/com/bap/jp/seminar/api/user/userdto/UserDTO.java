package com.bap.jp.seminar.api.user.userdto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * demo
 *
 * @author tuyennv
 * 7/6/2019
 */
@Getter
@Setter
@NoArgsConstructor
public class UserDTO {
    private String name;
    private String major;
}
