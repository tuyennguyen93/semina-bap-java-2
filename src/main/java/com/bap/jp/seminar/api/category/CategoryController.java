package com.bap.jp.seminar.api.category;

import com.bap.jp.seminar.common.exception.DataConflictException;
import com.bap.jp.seminar.common.exception.InternalServerException;
import com.bap.jp.seminar.common.exception.ResourceNotFoundException;
import com.bap.jp.seminar.common.model.Category;
import com.bap.jp.seminar.common.model.Topic;
import com.bap.jp.seminar.common.response.ErrorResponse;
import com.bap.jp.seminar.common.response.SuccessResponse;
import com.bap.jp.seminar.common.utils.SerminarUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * @author tuyennv
 * 7/6/2019
 */
@RestController
@RequestMapping("/categories")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    /**
     * find all category
     * code: API-18,API-16
     *
     * @return ResponseEntity
     */
    @GetMapping
    public ResponseEntity findAll() {
        return new ResponseEntity<>(categoryService.findAll(), HttpStatus.OK);
    }


    /**
     * find category by id
     * code: API-17,API-19
     *
     * @param id
     * @return
     */
    @GetMapping(value = {"/{id}"})
    public Category findById(@Valid @PathVariable("id") Integer id) throws Exception {
        return categoryService.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("category id '" + id + "' does no exist"));
    }

    /**
     * create a new category
     * @param category
     * @return
     * @throws DataConflictException
     */
    @PostMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity create(@Valid @RequestBody Category category) throws DataConflictException {
        if (!categoryService.existsByName(category.getName())) {
            Integer rs = categoryService.save(category);
            if (rs != -1) {
                SuccessResponse response = new SuccessResponse("Successfully created new category.");
                return new ResponseEntity<>(response, HttpStatus.CREATED);
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        throw new DataConflictException("Category is already in use: ");
    }

    /**
     * update category
     * @param id
     * @param category
     * @return
     */
    @PutMapping(value = {"/{id}"})
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity updateProfile(@Valid @PathVariable Integer id, @RequestBody Category category) {
        if (categoryService.update(id, category)) {
            SuccessResponse response = new SuccessResponse("Successfully update category.");
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        ErrorResponse response = new ErrorResponse("Category does no exist.");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping(value = {"/{id}"})
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity deleteById(@PathVariable("id") Integer categoryId) throws ResourceNotFoundException {
        SuccessResponse response;
        if (categoryService.findById(categoryId).isPresent()) {
            categoryService.deleteById(categoryId);
            response = new SuccessResponse("Successfully deleted Category id '" + categoryId + "'");
        } else{
            throw new ResourceNotFoundException("Category id '" + categoryId + "' does no exist");
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
