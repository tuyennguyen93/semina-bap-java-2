package com.bap.jp.seminar.api.schedule_register.ratedto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author tuyennv
 * 7/9/2019
 */
@Getter
@Setter
@NoArgsConstructor
public class RateDTO {
    private Integer scheduleId;
    private Integer rate;
}
