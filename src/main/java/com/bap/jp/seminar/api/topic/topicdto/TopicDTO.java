package com.bap.jp.seminar.api.topic.topicdto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author tuyennv
 * 7/8/2019
 */
@Getter
@Setter
@NoArgsConstructor
public class TopicDTO {
    private Short status;
    private String title;
    private String description;
    private Short type;

}
