package com.bap.jp.seminar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class SeminarinternApplicationApi {
	public static void main(String[] args) {
		SpringApplication.run(SeminarinternApplicationApi.class, args);
	}
}
