package com.bap.jp.seminar.common.model;

import com.bap.jp.seminar.common.utils.BaseModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="topic",uniqueConstraints = {
        @UniqueConstraint(columnNames = {"title"})
})
@Getter
@Setter
@NoArgsConstructor
public class Topic extends BaseModel {

  private Integer userId;

  private Short status;

  @NotNull
  @Size(min = 2,max=30, message = "Title should have  2->30 characters")
  private String title;

  @NotNull
  @Size(min = 2, message = "description should have atleast 2 characters")
  private String description;

  private Short type;

  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "category_topic",
          joinColumns = @JoinColumn(name = "topic_id"),
          inverseJoinColumns = @JoinColumn(name = "category_id"))
  Set<Category> categories = new HashSet<>();



}