package com.bap.jp.seminar.common.repository;

import com.bap.jp.seminar.api.schedule_register.ratedto.RateDTO;

import com.bap.jp.seminar.common.model.ScheduleRegister;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * @author tuyennv
 * 7/9/2019
 */

@Repository
@Transactional
public interface ScheduleRegisterRepository extends JpaRepository<ScheduleRegister, Integer> {
    @Modifying
    @Query(value = "UPDATE schedule_register SET rate=?1 WHERE user_id=?2 and schedule_id=?3", nativeQuery = true)
    void rate(Integer rate, Integer user_id, Integer schedule_id);

    @Query(value = "SELECT schedule_id,AVG(rate) AS rate from schedule_register GROUP BY schedule_id", nativeQuery = true)
    List<RateDTO> statisticalRates();

    @Query(value = "SELECT AVG(rate) from schedule_register WHERE schedule_id=?1", nativeQuery = true)
    Float statisticalRate(Integer idSchedule);

    Optional<ScheduleRegister> findByUserIdAndScheduleId(Integer userId, Integer scheduleId);

    List<ScheduleRegister> findAllByScheduleId(Integer scheduleId);

    List<ScheduleRegister> findAllByUserId(Integer scheduleId);


}
