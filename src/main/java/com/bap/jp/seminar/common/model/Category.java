package com.bap.jp.seminar.common.model;

import com.bap.jp.seminar.common.utils.BaseModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name="category",uniqueConstraints = {
        @UniqueConstraint(columnNames = {"name"})
})
public class Category extends BaseModel {

    @NotNull
    @Size(min = 2,max = 30,message = "Name should have  2->30 characters")
    private String name;

//    @OneToOne(mappedBy = "schedule")
//    private Schedule schedule;

}
