package com.bap.jp.seminar.common.model;

import com.bap.jp.seminar.common.utils.BaseModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;


/**
 * @author tuyennv
 * 7/9/2019
 */

@Entity
@Table(name="schedule_register")
@Getter
@Setter
@NoArgsConstructor
public class ScheduleRegister extends BaseModel {

    @Column(name = "user_id")
    private Integer userId;

    @Transient
    private String userName;

    @Column(name = "schedule_id")
    private Integer scheduleId;

    private Float rate;


}
