package com.bap.jp.seminar.common.exception;

public class DataConflictException extends Exception {

    private static final long serialVersionUID = 1L;

    public DataConflictException(String msg) {
        super(msg);
    }

    public DataConflictException(String msg, Throwable cause) {
        super(msg, cause);
    }
}