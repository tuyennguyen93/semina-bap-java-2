package com.bap.jp.seminar.common.model;

import com.bap.jp.seminar.common.utils.BaseModel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

/**
 * @author tuyennv
 * 7/7/2019
 */

@Entity
@Table(name="schedule")
@Getter
@Setter
@NoArgsConstructor
public class Schedule extends BaseModel {
    @NotNull
    @Size(min = 2,max=30, message = "Name should have  2->30 characters")
    private String speakerName;
   // private Integer topicId;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private  LocalDateTime timeStart;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime timeFinish;

    // ko set vao database
    @Transient
    private Float avgRate;

    @Transient
    private Boolean joiner;


    public void setTimeStart(String dateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime formatDateTime = LocalDateTime.parse(dateTime, formatter);
        this.timeStart = formatDateTime;
    }
    public void setTimeStart(LocalDateTime dateTime) {
        this.timeStart = dateTime;
    }

    public void setTimeFinish(String dateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime formatDateTime = LocalDateTime.parse(dateTime, formatter);
        this.timeFinish = formatDateTime;
    }
    public void setTimeFinish(LocalDateTime dateTime) {
        this.timeFinish = dateTime;
    }



    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "schedule_register",
            joinColumns = @JoinColumn(name = "schedule_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    Set<User> user = new HashSet<>();

    //test
    @OneToOne
    @JoinColumn(name = "topic_id")
    private Topic topic;


}
