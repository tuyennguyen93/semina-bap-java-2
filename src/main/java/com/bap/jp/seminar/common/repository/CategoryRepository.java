package com.bap.jp.seminar.common.repository;

import com.bap.jp.seminar.common.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * @author tuyennv
 * 7/6/2019
 */
@Repository
@Transactional
public interface CategoryRepository extends JpaRepository<Category,Integer> {
     Optional<Category> findByName(String name);
     boolean existsByName(String name);

}
