package com.bap.jp.seminar.common.utils;

import com.bap.jp.seminar.config.security.UserPrinciple;
import org.springframework.security.core.context.SecurityContextHolder;

public class SerminarUtils {
    /**
     * get id for current user
     *
     * @return
     */
    public static Integer getCurrentUserId() {
        try {
            UserPrinciple principle = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return principle.getId();
        } catch (Exception ex) {
            return null;
        }

    }

    public static Boolean isCurrentUserAdmin() {
        try {
            UserPrinciple principle = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (principle.getRole() == 1) return true;
            return false;
        } catch (Exception ex) {
            return false;
        }

    }

//    public static UserPrinciple getCurrentUserProfile(){
//        try{
//            UserPrinciple principle = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//            return principle;
//        } catch (Exception ex){
//            return null;
//        }
//
//    }

}
