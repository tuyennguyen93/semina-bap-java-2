package com.bap.jp.seminar.common.utils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author tuyennv
 * 7/10/2019
 */
@MappedSuperclass
@Getter
@Setter
@NoArgsConstructor
public abstract class BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    protected Integer id;

    @Column(name = "created")
    @CreationTimestamp
    public LocalDateTime created;

    @Column(name = "updated")
    @UpdateTimestamp
    public LocalDateTime updated;
}
