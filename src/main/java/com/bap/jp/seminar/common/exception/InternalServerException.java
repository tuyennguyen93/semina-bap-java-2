package com.bap.jp.seminar.common.exception;

public class InternalServerException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public InternalServerException(String msg) {
        super(msg);
    }

    public InternalServerException(String msg, Throwable cause) {
        super(msg, cause);
    }
}