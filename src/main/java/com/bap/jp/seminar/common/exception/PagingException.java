package com.bap.jp.seminar.common.exception;

/**
 * @author tuyennv
 * 7/17/2019
 */
public class PagingException extends Exception {

    public PagingException(String msg) {
        super(msg);
    }

    public PagingException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
