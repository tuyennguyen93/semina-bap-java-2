package com.bap.jp.seminar.common.repository;


import com.bap.jp.seminar.common.model.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * @author tuyennv
 * 7/9/2019
 */
@Repository
public interface ScheduleRepository extends JpaRepository<Schedule,Integer> {
}
