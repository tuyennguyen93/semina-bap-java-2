package com.bap.jp.seminar.common.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
public class SuccessResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    private String message;
    private Object data;

    public SuccessResponse(String message) {
        this.message = message;
    }
}
