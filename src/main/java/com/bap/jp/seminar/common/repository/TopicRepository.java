package com.bap.jp.seminar.common.repository;

import com.bap.jp.seminar.common.model.Topic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface  TopicRepository extends JpaRepository<Topic,Integer> {

    Optional<Topic> findByTitle(String title);

    List<Topic> findAllByStatus(Short status);

    List<Topic> findAllByUserId(Integer id);

    boolean existsByTitle(String title);
}
